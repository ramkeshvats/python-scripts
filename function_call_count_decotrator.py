
class CountCalls(object):
    """
    Decorator that keeps track of the number of times a function is called.
    """
    __instances = {}

    def __init__(self, f):
        self.__f = f
        self.__num_calls = 0
        CountCalls.__instances[f] = self

    def __call__(self, *args, **kwargs):
        self.__num_calls += 1
        return self.__f(*args, **kwargs)

    def count(self):
        return CountCalls.__instances[self.__f].__numcalls

    @staticmethod
    def counts():
        return dict(
            [(f.__name__, CountCalls.__instances[f].__num_calls) for f in
             CountCalls.__instances])


@CountCalls
def f():
    print 'Called f'


@CountCalls
def g():
    print 'Called g'

f()
f()
f()
g()
print 'F function calls: ', f.count()
print 'G function calls: ', g.count()
